package lanceur;

import com.objis.demojson.domaine.Employe;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.File;
import java.io.FileOutputStream;

public class DemoJsonJackson {
public static void main(String[] args) throws IOException{
	
	ObjectMapper mapper = new ObjectMapper();
	Employe employe1 = new Employe("uuygiuliu","Amadou");

	//Object vers fichier JSON
	File resultFile = new File("employe.json");
	//mapper.writeValue(resultFile, employe1);
	JsonGenerator jg = mapper.getFactory()
			.createGenerator(new FileOutputStream(resultFile));
	jg.writeStartArray();
	mapper.writeValue(jg,employe1);
	mapper.writeValue(jg,employe1);
	mapper.writeValue(jg,employe1);
	jg.writeEndArray();
	jg.close();
	}
}
